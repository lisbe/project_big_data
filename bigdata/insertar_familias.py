#!/usr/bin/python3
import csv
import os
import sys
import hashlib
import happybase


def execute(f, c):
    csvfile1 = open('%s/SET-dec-2013.csv' % os.getcwd(), 'rb')
    rows1 = csv.reader(csvfile1, delimiter=',', quotechar='|')
    families = {'SensorD': {}}
    cont = 1
    while cont <= c:
        families.update({"Medidas%d%d" % cont: {}})
        cont += 1
    conn = happybase.Connection(host="master.krejcmat.com", port=9090)
    conn.open()
    tables = conn.tables()
    if "Sensores" in tables:
        table = conn.table('Sensores')
    else:
        conn.create_table('Sensores', families)
        table = conn.table('Sensores')
    batch = table.batch(batch_size=1000)
    cont = 1
    while cont <= f:
        cont += 1
        for line1, line2 in zip(rows1, rows2):
            row_key = hashlib.md5(line1[0] + line1[1].split(' ')[0]).hexdigest()
            SensorD = {'SensorD:Sensor': str(f) + line1[0],
                          'SensorD:Fecha': line1[1].split(' ')[0]}
            cont1 = 1
            while cont1 <= c:
                SensorD.update({"Medidas%d:%s" % (cont1, line1[1].split(' ')[1]): line1[2]})
                cont1 += 1
            batch.put(row_key, SensorD)
            print "Se ha insertado la llave:%s y los valores:%s" % (row_key, str(SensorD))
            row_key = hashlib.md5(line2[0]+ line2[1].split(' ')[0]).hexdigest()
            SensorD = {'SensorD:Sensor': str(f) + line2[0],
                          'SensorD:Fecha': line2[1].split(' ')[0]}
            cont1 = 1
            while cont1 <= c:
                SensorD.update({"Medidas%d:%s" % (cont1, line2[1].split(' ')[1]): line2[2]})
                cont1 += 1
            batch.put(row_key, SensorD)
            print "Se ha insertado la llave:%s y los valores:%s" % (row_key, str(SensorD))
            batch.send()
    conn.close()


if __name__ == "__main__":
    arg = sys.argv
    execute(int(arg[1]), int(arg[2]))
