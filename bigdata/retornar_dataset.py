#!/usr/bin/python3
import csv
import happybase
import os
import sys
import datetime
from dateutil.relativedelta import relativedelta


def execute(f, c):
    with open('%s/output.csv' % os.getcwd(), 'wb') as csvfile:
        conn = happybase.Connection(host="master.krejcmat.com", port=9090)
        conn.open()
        table = conn.table('Sensores')
        print "Conexion establecida"
        values = {}
        headder_write = False
        row_key = ''
        filter = b"SingleColumnValueFilter ('SensorD', 'Sensor',=, 'regexstring:^"+str(f)+"DG')"
        families = ['SensorD', "Medidas%d" % c]
        headder = ["Sensor", "Date"]
        for key, data in table.scan(filter=filter, columns=['SensorD', 'Medidas'+str(c)], sorted_columns=True):
            row_key = key
            values = data
            if not headder_write:
                first_date = datetime.datetime.strptime(values.get("%s:%s" %(families[0], headder[1])), "%Y-%m-%d")
                minutes_lis = [first_date.time().strftime("%H:%M")]
                next_date = first_date
                while next_date.date() == first_date.date():
                    next_date += relativedelta(minutes=+10)
                    if next_date.time().strftime("%H:%M") in minutes_lis:
                        break
                    minutes_lis.append(next_date.time().strftime("%H:%M"))
            result = csv.DictWriter(csvfile, fieldnames=headder + minutes_lis)
            if not headder_write:
                result.writeheader()
                headder_write = True
                print "Escribiendo las cabeceras en el csv"
            res_dict = {x: values.get("%s:%s" % (families[0], x)) for x in headder}
            res_dict.update({x: values.get("%s:%s" % (families[1], x)) or -99999999 for x in minutes_lis})
            print "Escribiendo los valores:%s en el csv" % str(res_dict)
            result.writerow(res_dict)
        conn.close()


if __name__ == "__main__":
    arg = sys.argv
    execute(int(arg[1]), int(arg[2]))
